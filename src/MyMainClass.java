public class MyMainClass {

        static int max_size= 20;
        static int max_k =20;

        static int [][]dp = new int[max_size][max_k];

        static boolean [][]v = new boolean[max_size][max_k];

        static int sum = 0;

        static void findSum(int arr[], int n)
        {
            for (int i = 0; i < n; i++)
                sum += arr[i];
        }

        static int cntWays(int arr[], int i, int kk,
        int k, int n, int curr_sum)
        {

            if (sum % k != 0)
                return 0;
            if (i != n && kk == k + 1)
                return 0;

            if (i == n)
            {
                if (kk == k + 1)
                    return 1;
                else
                    return 0;
            }
            if (v[i][kk])
                return dp[i][kk];

            curr_sum += arr[i];


            v[i][kk] = true;


            dp[i][kk] = cntWays(arr, i + 1, kk, k, n, curr_sum);
            if (curr_sum == (sum / k) *kk)
                dp[i][kk] += cntWays(arr, i + 1, kk + 1, k, n, curr_sum);


            return dp[i][kk];
        }

// Driver code
        public static void main(String[] args)
        {
            int arr[] = { 2,3,2,3,2,3,2,3};
            int n = arr.length;
            int k = 2;


            findSum(arr, n);


            System.out.println(cntWays(arr, 0, 1, k, n, 0));
        }
    }
